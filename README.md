# Rapid Reproducible R Project

> Demonstrate a rapid reproducible workflow on the Kaggle Future Sales competition

Welcome to the Rapid Reproducible R project.

## Getting Started

### Setup

We're using two main R packages to control environment and workflow.

* [renv](https://github.com/rstudio/renv) as a package manager
* [drake](https://ropensci.github.io/drake/) as a pipeline manager

After cloning the repo and opening RStudio renv should whizz into action creating an isolated package environment. The the first thing you should do is the following:

1. Restart R to make sure it's picked up renv's environment.
2. Run `renv::restore()` to install the dependencies.

If you need extra packages you can install them the normal way and update the `renv.lock` file.

Drake will also be installed by renv. If you want to contribtue to the pipeline code then you should probably check out the [full Drake manual](https://ropenscilabs.github.io/drake-manual/).

### Data

Download all the data from the "Predict Future Sales" kaggle competition. You'll need to login to Kaggle, go to the competition page:

[https://www.kaggle.com/c/competitive-data-science-predict-future-sales](https://www.kaggle.com/c/competitive-data-science-predict-future-sales)

and on the data tab hit download all. If you're already logged in this link should work:

[https://www.kaggle.com/c/8587/download-all](https://www.kaggle.com/c/8587/download-all)

Note. Since I started this project the zip file changed a bit. Use the latest. Save the output to `data/competitive-data-science-predict-future-sales.zip`. Then run the drake pipeline.

```r
library(drake)
r_make()
```

This checks the md5 of the zip and unpacks to the right place, which is `data/raw`. Please don't touch this data, or add it to the repo. This needs to be the same for everyone. Then each file is read and the R objects stored in your local cache. You can list them with `cached` and you can load them using the `loadd`/`readd` function:

```r
cached()
```
```
[1] "item_categories"   "items"             "raw_files"         "sales_test"       
[5] "sales_train"       "sample_submission" "shops" 
```

```r
loadd(items)
```

Try to start from these data frames rather than the raw csvs. If you need to go back to the raw then consider upgrading the pipeline afterwards.

## Layout

General layout is below:

```
+-- data           Store data here. Try to avoid committing
|   \-- raw        Where the raw data zip is expanded to
+-- lib            Supporting packages
+-- notebooks      Telling stories, adhoc analysis. Link to an issue
+-- R              Pipeline scripts for building main datasets
+-- scripts        Useful, general scripts that are not part of the package
+-- renv           Package environment. No need to directly interact with
+-- _drake.R       Main data pipeline script
```

### `lib`

The project package/packages live in here.

### `notebooks`

We'll make a lot of notebooks reporting analysis and trying things out. They live in here. Ideally they'd link to an issue in the project repo and by linking names and commits we can refer back and forth between them.

See [notebooks/00-getting-started.ipynb](notebooks/00-getting-started.ipynb) for an example of a Jupyter notebook and [notebooks/00-getting-started.Rmd](notebooks/00-getting-started.Rmd) for an RStudio notebook.

### `R`

The main data pipeline involves a few scripts that don't go into the package. They live here. Consider this directory "production", as opposed to scripts, which is more experimental.

### `scripts`

Blocks of R code that we want to keep but either aren't ready of the package or the R pipeline code. This is similar to notebooks except that it's chunks of code, functions, and so on.


