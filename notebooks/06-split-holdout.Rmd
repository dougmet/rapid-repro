---
title: "Split the Holdout"
output: html_notebook
---

While we will probably do some form of backtest it's useful to holdout one month so that we can evaluate our early models. The final prediction for the competition is November 2015. We'll hold out October 2015.

Let's update the pipeline.

```{r, message=FALSE, echo=FALSE}
library(drake)
library(tidyverse)
library(tsibble)
library(here)
setwd(here())

r_make()

loadd("sales_train")
```

The `sales_train` data has a `date_block_num` column, which is for splitting into year-months. We'll use this to split the holdout.

```{r}
max(sales_train$date_block_num)
```

So there are 33 months. Splitting off one shouldn't hurt too much.

## De-deuping

As discussed in `03-sales-train.ipynb` the time series has duplication in some rare cases. Because they're so small we clean them out with the following strategy.

* Sum the `item_cnt_day`.
* Make a weight average of the `item_price`.
* If the summed day count is zero then set the price to zero and remove the row.

```{r}
sales_unique <- sales_train %>%
    group_by(date, shop_id, item_id) %>%
    summarise(
      date_block_num = first(date_block_num),
      item_price = ifelse(
        sum(item_cnt_day) == 0,
        0,
        sum(item_price * item_cnt_day) / sum(item_cnt_day)
      ),
      item_cnt_day = sum(item_cnt_day)
    ) %>%
    filter(item_cnt_day > 0)

dim(sales_unique)
```

## Tsibble

I've decided to try out tsibbles. They are basically tibbles but they provide some nice window functions that I think could be useful for us. We'll see how it goes. The act of creating a tsibble involves a big `distinct` operation (and possibly a sort). But I like that it guarantees a few things that you might assume about a time series.

1. Time ordered.
2. Uniquely defined in index and keys.

In our case the index is date and the keys are `shop_id` and `item_id`.

```{r}
system.time({
  sales_ts <- as_tsibble(sales_unique, index = date, key = c(shop_id, item_id))
})
```

But now we can do things like:

```{r}
sales_ts %>%
  index_by(yearmon = yearmonth(date)) %>%
  group_by(yearmon) %>%
  summarise(item_cnt_month = sum(item_cnt_day)) %>%
  ggplot(aes(x=yearmon, y = item_cnt_month)) +
  geom_line()
```

Which now I see it could easily be achieved with dplyr! OK, well not to worry, still sticking with it for now.

## Split

Easy enough from here. Using the `date_block_num`.

```{r}
last_block <- max(sales_ts$date_block_num)
sales_ts_test <- sales_ts %>%
  filter(date_block_num == last_block)
```

So here is out holdout target

```{r}
summary(sales_ts_test$item_cnt_day)
```

```{r}
ggplot(sales_ts_test, aes(x=log10(item_cnt_day))) + geom_histogram(bins=100)
```