# Contributing Guidlines

## Issue and Notebooks

Grab an issue from the issues board, make a branch and get cracking. Ideally when you submit back, include a notebook in the notebooks directory explaining what you did and your research.

If using RStudio notebooks be sure to check in the .nb.html file so that it can be viewed by others. For jupyter notebooks check in the whole notebook.

## Branches

The master branch cannot be pushed to directly. Work on a feature branch and submit a merge request. We are roughly using [GitHub Flow](https://guides.github.com/introduction/flow/index.html). With feature branches, master is the development head, and release tags for releases/submissions.

Use branches that related to a new feature or an issue. Ideally connect a branch with an issue directly by mentioning it in a comment (`this commit relates to #4`). When the feature is done submit the merge request, make any requested changes, merge in, and then delete the branch.

Avoid personal branches (`dougs-branch`) and branches that get abandoned for a long time.

## Code Style

[Tidyverse style guide](https://style.tidyverse.org/) for R

Try [lintr](https://github.com/jimhester/lintr) for help. Also `ctrl-shift-A` in RStudio for general formatting.

[PEP 8](https://www.python.org/dev/peps/pep-0008/) for Python

Consider the [flake8 linter](http://flake8.pycqa.org/en/latest/) for help.
